# ArmuroL432

## Modified files:

- `Core/Src/ADC/*`
- `Core/Src/Buttons/*`
- `Core/Src/Encoder/*`
- `Core/Src/Led/*`
- `Core/Src/LightSensors/*`
- `Core/Src/Log/*`
- `Core/Src/Motors/*`
- `Core/Src/Robot/*`
- `Core/Src/Timer/*`
- `Core/Src/Util/*`
- `Core/Src/main.c`
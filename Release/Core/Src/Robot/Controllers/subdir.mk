################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/Robot/Controllers/ParcoursControllerMk1.c \
../Core/Src/Robot/Controllers/ParcoursControllerMk2.c 

OBJS += \
./Core/Src/Robot/Controllers/ParcoursControllerMk1.o \
./Core/Src/Robot/Controllers/ParcoursControllerMk2.o 

C_DEPS += \
./Core/Src/Robot/Controllers/ParcoursControllerMk1.d \
./Core/Src/Robot/Controllers/ParcoursControllerMk2.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/Robot/Controllers/%.o Core/Src/Robot/Controllers/%.su: ../Core/Src/Robot/Controllers/%.c Core/Src/Robot/Controllers/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DUSE_HAL_DRIVER -DSTM32L432xx -c -I../Core/Inc -I../Drivers/STM32L4xx_HAL_Driver/Inc -I../Drivers/STM32L4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32L4xx/Include -I../Drivers/CMSIS/Include -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Src-2f-Robot-2f-Controllers

clean-Core-2f-Src-2f-Robot-2f-Controllers:
	-$(RM) ./Core/Src/Robot/Controllers/ParcoursControllerMk1.d ./Core/Src/Robot/Controllers/ParcoursControllerMk1.o ./Core/Src/Robot/Controllers/ParcoursControllerMk1.su ./Core/Src/Robot/Controllers/ParcoursControllerMk2.d ./Core/Src/Robot/Controllers/ParcoursControllerMk2.o ./Core/Src/Robot/Controllers/ParcoursControllerMk2.su

.PHONY: clean-Core-2f-Src-2f-Robot-2f-Controllers


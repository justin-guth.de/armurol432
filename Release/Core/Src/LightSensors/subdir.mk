################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/LightSensors/LightSensors.c 

OBJS += \
./Core/Src/LightSensors/LightSensors.o 

C_DEPS += \
./Core/Src/LightSensors/LightSensors.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/LightSensors/%.o Core/Src/LightSensors/%.su: ../Core/Src/LightSensors/%.c Core/Src/LightSensors/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DUSE_HAL_DRIVER -DSTM32L432xx -c -I../Core/Inc -I../Drivers/STM32L4xx_HAL_Driver/Inc -I../Drivers/STM32L4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32L4xx/Include -I../Drivers/CMSIS/Include -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Src-2f-LightSensors

clean-Core-2f-Src-2f-LightSensors:
	-$(RM) ./Core/Src/LightSensors/LightSensors.d ./Core/Src/LightSensors/LightSensors.o ./Core/Src/LightSensors/LightSensors.su

.PHONY: clean-Core-2f-Src-2f-LightSensors


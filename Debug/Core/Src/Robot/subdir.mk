################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/Robot/Controller.c \
../Core/Src/Robot/Launch.c 

OBJS += \
./Core/Src/Robot/Controller.o \
./Core/Src/Robot/Launch.o 

C_DEPS += \
./Core/Src/Robot/Controller.d \
./Core/Src/Robot/Launch.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/Robot/%.o Core/Src/Robot/%.su: ../Core/Src/Robot/%.c Core/Src/Robot/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32L432xx -c -I../Core/Inc -I../Drivers/STM32L4xx_HAL_Driver/Inc -I../Drivers/STM32L4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32L4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Src-2f-Robot

clean-Core-2f-Src-2f-Robot:
	-$(RM) ./Core/Src/Robot/Controller.d ./Core/Src/Robot/Controller.o ./Core/Src/Robot/Controller.su ./Core/Src/Robot/Launch.d ./Core/Src/Robot/Launch.o ./Core/Src/Robot/Launch.su

.PHONY: clean-Core-2f-Src-2f-Robot


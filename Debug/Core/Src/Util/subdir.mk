################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/Util/Util.c 

OBJS += \
./Core/Src/Util/Util.o 

C_DEPS += \
./Core/Src/Util/Util.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/Util/%.o Core/Src/Util/%.su: ../Core/Src/Util/%.c Core/Src/Util/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32L432xx -c -I../Core/Inc -I../Drivers/STM32L4xx_HAL_Driver/Inc -I../Drivers/STM32L4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32L4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Src-2f-Util

clean-Core-2f-Src-2f-Util:
	-$(RM) ./Core/Src/Util/Util.d ./Core/Src/Util/Util.o ./Core/Src/Util/Util.su

.PHONY: clean-Core-2f-Src-2f-Util


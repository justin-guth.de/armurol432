################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/Log/Log.c 

OBJS += \
./Core/Src/Log/Log.o 

C_DEPS += \
./Core/Src/Log/Log.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/Log/%.o Core/Src/Log/%.su: ../Core/Src/Log/%.c Core/Src/Log/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32L432xx -c -I../Core/Inc -I../Drivers/STM32L4xx_HAL_Driver/Inc -I../Drivers/STM32L4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32L4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Src-2f-Log

clean-Core-2f-Src-2f-Log:
	-$(RM) ./Core/Src/Log/Log.d ./Core/Src/Log/Log.o ./Core/Src/Log/Log.su

.PHONY: clean-Core-2f-Src-2f-Log


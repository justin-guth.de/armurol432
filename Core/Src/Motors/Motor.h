#ifndef MOTORS_H
#define MOTORS_H

#include <stdint.h>

typedef enum { LeftMotor = 0b01, RightMotor = 0b10} Motors;
typedef enum { Forward, Backward, Standstill} MotorDirection;

uint32_t GetPWMRatio(float velocity, int invert);
void SetMotorState(Motors motors, MotorDirection direction, float velocity);



#endif

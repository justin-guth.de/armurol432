#include "Motor.h"

#include "../Definitions.h"
#include "../Util/Util.h"


/**
 * Compute an integer CCR value for a given normalized percentage (between 0 and 1)
 * depending on which motor is later accessed the invert flag may be set
 */
uint32_t GetPWMRatio(float velocity, int invert) {

	// Clamp the input velocity between 0 and 1
	velocity = ClampUnit(velocity);

	// Get the range of possible values implicitly from 0 up to 2^16
	const static uint32_t valueRange = (1 << 16) - 1;

	// Depending on whether the robot is driving forward or backward the given velocity may need to be inverted
	if (invert)
	{
		return (uint32_t) ((1.0f - velocity) * valueRange);
	}
	else
	{
		return (uint32_t) (velocity * valueRange);
	}
}

/**
 * Sets one or more motors to a given direction and velocity
 *
 * Note: For my robot at some point the function of CCR2 and CCR3 swapped.
 * 	     I do not know what caused this. It may be necessary to swap them when running this code on another robot
 * 	     (and possibly even invert the invert flags)
 *
 */
void SetMotorState(Motors motors, MotorDirection direction, float velocity)
{
	if (motors & RightMotor) {

		switch(direction) {

			case Forward:

				HAL_GPIO_WritePin(RIGHT_PHASE2_GPIO_PORT, RIGHT_PHASE2_GPIO_PIN, GPIO_PIN_RESET);
				TIM1->CCR3 = GetPWMRatio(velocity, 0);
				break;

			case Backward:

				HAL_GPIO_WritePin(RIGHT_PHASE2_GPIO_PORT, RIGHT_PHASE2_GPIO_PIN, GPIO_PIN_SET);
				TIM1->CCR3 = GetPWMRatio(velocity, 1);
				break;

			case Standstill:

				HAL_GPIO_WritePin(RIGHT_PHASE2_GPIO_PORT, RIGHT_PHASE2_GPIO_PIN, GPIO_PIN_RESET);
				TIM1->CCR3 = GetPWMRatio(0.0f, 0);
				break;
		}
	}

	if (motors & LeftMotor) {

		switch(direction) {
			case Forward:

				HAL_GPIO_WritePin(LEFT_PHASE2_GPIO_PORT, LEFT_PHASE2_GPIO_PIN, GPIO_PIN_SET);
				TIM1->CCR2 = GetPWMRatio(velocity, 1);
				break;

			case Backward:

				HAL_GPIO_WritePin(LEFT_PHASE2_GPIO_PORT, LEFT_PHASE2_GPIO_PIN, GPIO_PIN_RESET);
				TIM1->CCR2 = GetPWMRatio(velocity, 0);
				break;

			case Standstill:

				HAL_GPIO_WritePin(LEFT_PHASE2_GPIO_PORT, LEFT_PHASE2_GPIO_PIN, GPIO_PIN_SET);
				TIM1->CCR2 = GetPWMRatio(0.0f, 1);
				break;
		}
	}

}

#include "../IO/IO.h"

#include "stm32l4xx_hal.h"
#include "usart.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdint.h>

#define STRIDE 4

static volatile uint8_t buffer[256];
static volatile uint8_t readBuffer[256];
static volatile uint32_t bufferWriteIndex = 0;
static volatile uint32_t lastBufferWriteIndex = 0;

/**
 * Output a formatted string on the serial bus
 */
void Log(const char* format, ...) {

	// write variadic arguments into variable
	va_list arguments;
	va_start(arguments, format);

	// prepare buffer
	static char buffer[256];

	// format string using variadic arguments and store length of resulting string
	int length = vsprintf(buffer, format, arguments);

	// transmit buffer contents on the serial bus
	HAL_UART_Transmit(&huart2,(uint8_t*) buffer, length, (uint32_t) (1000000));
}

void USART2_IRQHandler(void)
{
   HAL_UART_IRQHandler(&huart2);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  if (huart->Instance == USART2)
  {
	/*Log("Received: ", (char*) buffer);

	for (int i = 0; i < 32; i++) {
		Log("%02X ", buffer[i]);
	}

	Log("\n");*/

	if (buffer[(bufferWriteIndex + 255) % 256] == 0x0D) {
		int destinationBufferIndex = 0;
		for (int i = lastBufferWriteIndex; i != bufferWriteIndex; i = (i + 1) % 256) {
			readBuffer[destinationBufferIndex] = buffer[i];
			destinationBufferIndex ++;
		}
		readBuffer[destinationBufferIndex - 1] = 0x00;
		lastBufferWriteIndex = bufferWriteIndex;
		Log("Received: %s\n", (char*) readBuffer);
	}
  }
}

void TryRead() {

	HAL_StatusTypeDef status = HAL_UART_Receive_IT(&huart2, (uint8_t*) &buffer[bufferWriteIndex], STRIDE);

	if (status == HAL_OK) {
		bufferWriteIndex = (bufferWriteIndex + STRIDE) % 256;
	}
}



void WriteReceived(uint8_t* buffer, uint32_t size) {

	for (int i = 0; i < 256 && i < size; i++) {

		buffer[i] = readBuffer[i];
	}
}


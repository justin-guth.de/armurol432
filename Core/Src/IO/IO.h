#ifndef LOG_H
#define LOG_H

#include <stdint.h>

void Log(const char* format, ...);
void TryRead();
void WriteReceived(uint8_t* buffer, uint32_t size);


#endif

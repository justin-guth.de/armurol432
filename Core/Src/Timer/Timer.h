#ifndef TIMER_H
#define TIMER_H

#include <stdint.h>

struct TimerStruct {

	uint32_t lastResetTick;
	uint32_t tickCount;
	uint32_t lastOnTimeElapsedTick;
};

typedef struct TimerStruct Timer;


void ResetTimer(struct TimerStruct* timer);

int OnTimeElapsedCondition(struct TimerStruct* timer, uint32_t milliseconds);
int UntilTimeElapsedCondition(struct TimerStruct* timer, uint32_t milliseconds);
int OnTimeElapsedPeriodicCondition(struct TimerStruct* timer, uint32_t milliseconds);
int AfterTimeElapsedCondition(struct TimerStruct* timer, uint32_t milliseconds);
uint32_t GetElapsedTime(struct TimerStruct* timer);

// Define some quality of life macros. instead of manually checking the conditions one can now write:
// OnTimeElapsed(&timer, 1000) { /* do something...*/ }
#define OnTimeElapsed(TIMER_P, MILLISECONDS) if (OnTimeElapsedCondition(TIMER_P, MILLISECONDS))
#define UntilTimeElapsed(TIMER_P, MILLISECONDS) if (UntilTimeElapsedCondition(TIMER_P, MILLISECONDS))
#define OnTimeElapsedPeriodic(TIMER_P, MILLISECONDS) if (OnTimeElapsedPeriodicCondition(TIMER_P, MILLISECONDS))
#define AfterTimeElapsed(TIMER_P, MILLISECONDS) if (AfterTimeElapsedCondition(TIMER_P, MILLISECONDS))

#endif

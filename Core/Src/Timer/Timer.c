#include "Timer.h"

#include "stm32l4xx_hal.h"

/**
 * Resets a given timer
 */
void ResetTimer(struct TimerStruct* timer)
{
	timer->lastResetTick = HAL_GetTick();
	timer->tickCount = 0;

	// set to 0b111...111
	timer->lastOnTimeElapsedTick = ~0;
}

/**
 * returns wether a certain amount of time has elapsed for a given timer and wether the timer has not ticked yet.
 * This is used for one time events that happen after a given time has elapsed.
 */
int OnTimeElapsedCondition(struct TimerStruct* timer, uint32_t milliseconds)
{
	// If time since last reset surpasses the given amount of milliseconds
	if (HAL_GetTick() - timer->lastResetTick > milliseconds) {

		// If last elapsed tick is set and last elapsed tick is greater than the given milliseconds return false
		if (timer->lastOnTimeElapsedTick != ~0 && timer->lastOnTimeElapsedTick >= milliseconds) {
			return 0;
		}

		// update timer data and return true
		timer->lastOnTimeElapsedTick = milliseconds;
		timer->tickCount++;
		return 1;
	}

	// else return false
	return 0;
}

/**
 * returns wether a certain amount of time has not yet elapsed for a given timer.
 * This is used for repeating events that happen until a given time has elapsed.
 */

int UntilTimeElapsedCondition(struct TimerStruct* timer, uint32_t milliseconds) {

	// If time since last reset does not surpass the given amount of milliseconds
	if (HAL_GetTick() - timer->lastResetTick < milliseconds)
	{
		// tick and return true
		timer->tickCount++;
		return 1;
	}

	// else return false
	return 0;
}


/**
 * returns wether a certain amount of time has elapsed for a given timer periodically.
 * This is used for repeating events that happen in specific intervals.
 */
int OnTimeElapsedPeriodicCondition(struct TimerStruct* timer, uint32_t milliseconds)
{
	// if time since last reset tick surpasses the given milliseconds
	if (HAL_GetTick() - timer->lastResetTick > milliseconds) {

		// tick and reset the last tick. return true
		timer->tickCount++;
		timer->lastResetTick = HAL_GetTick();
		return 1;
	}

	// else return false
	return 0;
}

/**
 * returns wether a certain amount of time has elapsed for a given timer.
 * This is used for repeating events that happen after a given time has elapsed.
 */
int AfterTimeElapsedCondition(struct TimerStruct* timer, uint32_t milliseconds) {

	// If time since last reset does surpass the given amount of milliseconds
	if (HAL_GetTick() - timer->lastResetTick > milliseconds)
	{
		// tick and return true
		timer->tickCount++;
		return 1;
	}

	// else return false
		return 0;
}

/**
 * get the elapsed time for a given timer
 */
uint32_t GetElapsedTime(struct TimerStruct* timer) {

	return HAL_GetTick() - timer->lastResetTick;
}

#include "stm32l4xx_hal.h"

#include "AnalogDigitalConverter.h"
#include "adc.h"


// Stores the current ADC values
volatile static uint32_t adc[6];

// Buffer for the ADC to write values into
uint32_t buffer[6];

// Whether the current conversion of ADC values is done
volatile uint8_t IsConversionDone = 1;

/**
 * If no conversion is running currently request new ADC conversion reading
 */
void TryUpdateADCValues() {

	if (IsConversionDone)
	{
		IsConversionDone = 0;
		HAL_ADC_Start_DMA(&hadc1, buffer, 6);
	}
}

/**
 * returns an ADC value at a given position in the array
 */
uint32_t GetAdcValue(uint32_t index)
{
	return adc[index];
}

/**
 * Handles conversion completion
 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc1)
{
	// Copy values from the newly updated ADC buffer to the general ADC buffer
	for (int i =0; i<6; i++)
	{
	  adc[i] = buffer[i];
	}

	// Set flag to true so new Update requests can be made
	IsConversionDone = 1;
}

#ifndef ADC_H
#define ADC_H

#include <stdint.h>

void TryUpdateADCValues();
uint32_t GetAdcValue(uint32_t index);

#endif

#ifndef UTIL_H
#define UTIL_H

#include "stm32l4xx_hal.h"

void Wait(uint32_t milliseconds);
float Abs(float a);
int Sign(float a);
float Min(float a, float b);
float Max(float a, float b);
float Clamp(float value, float low, float high);
float ClampUnit(float value);
float Sqrt(float a);


#endif

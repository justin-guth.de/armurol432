#include "Util.h"

/**
 * Wait a given amount of time
 * basically adapts to HAL_Delay but is easier to read and write.
 */
void Wait(uint32_t milliseconds)
{
	HAL_Delay(milliseconds);
}

/**
 * return the absolute value of a given float
 */
float Abs(float a) {

	return a < 0 ? -a : a;
}

/**
 * return the sign of a given float
 */
int Sign(float a) {

	return a < 0 ? -1 : 1;
}

/**
 * return the lower of two floats
 */
float Min(float a, float b)
{
	return a > b ? b : a;
}

/**
 * return the higher of two floats
 */
float Max(float a, float b)
{
	return a > b ? a : b;
}

/**
 * clamp a value between a lower and a higher bound
 */
float Clamp(float value, float low, float high)
{
	return Min(Max(value, low), high);
}

/**
 * clamp a value between a 0 and 1
 */
float ClampUnit(float value)
{
	return Clamp(value, 0.0f, 1.0f);
}

/**
 * Compute square root of a number
 */
float Sqrt(float a) {

	float result = a;
	float precision = 0.01f;

	while (Abs(a - result * result) > precision) {
		result = (result + a / result) / 2.0f;
	}

	return result;
}


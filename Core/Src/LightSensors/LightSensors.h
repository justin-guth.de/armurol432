#ifndef LIGHT_SENSORS_H
#define LIGHT_SENSORS_H

#include <stdint.h>

typedef enum { LeftLightSensor, CenterLightSensor, RightLightSensor } LightSensor;

void CalibrateLightSensors();
void CalibrateLightSensorsValues(int32_t newLeftLow, int32_t newLeftHigh, int32_t newCenterLow, int32_t newCenterHigh, int32_t newRightLow, int32_t newRightHigh);

uint32_t GetLightSensorValue(LightSensor sensor);
float GetUnbufferedNormalizedLightSensorValue(LightSensor sensor);
float GetNormalizedLightSensorValue(LightSensor sensor);
void WriteLightSensorValuesTo(uint32_t* buffer);
void WriteNormalizedLightSensorValuesTo(float* buffer);

void UpdateNormalizedLightSensorValues();





#endif


#include "LightSensors.h"
#include "../ADC/AnalogDigitalConverter.h"

#include "../Util/Util.h"
#include "../Buttons/Buttons.h"
#include "../IO/IO.h"

static int32_t leftLow = 0;
static int32_t leftHigh = (1 << 12);

static int32_t centerLow = 0;
static int32_t centerHigh = (1 << 12);

static int32_t rightLow = 0;
static int32_t rightHigh = (1 << 12);

static int32_t leftRange = (1 << 12);
static int32_t centerRange = (1 << 12);
static int32_t rightRange = (1 << 12);

static float normalizedLeft = 0.0f;
static float normalizedCenter = 0.0f;
static float normalizedRight = 0.0f;

/**
 * Calibrates LightSensors for Normalization usage
 */
void CalibrateLightSensors()
{
	Log("Calibration started, please wait.\n");
	Wait(2000);
	Log("Please place the left light sensor on a dark surface. Then press the left button.\n");
	while(!(GetPressedButtons() & LeftButton)) TryUpdateADCValues();
	leftLow = GetLightSensorValue(LeftLightSensor);

	Log("Please place the center light sensor on a dark surface. Then press the center button.\n");
	while(!(GetPressedButtons() & CenterButton)) TryUpdateADCValues();
	centerLow = GetLightSensorValue(CenterLightSensor);


	Log("Please place the right light sensor on a dark surface. Then press the right button.\n");
	while(!(GetPressedButtons() & RightButton)) TryUpdateADCValues();
	rightLow = GetLightSensorValue(RightLightSensor);

	Log("Please place the robot on a light surface. Then press the center button.\n");
	while(!(GetPressedButtons() & CenterButton)) TryUpdateADCValues();
	leftHigh = GetLightSensorValue(LeftLightSensor);
	centerHigh = GetLightSensorValue(CenterLightSensor);
	rightHigh = GetLightSensorValue(RightLightSensor);

	Log("Calibration complete. The calibrated value ranges are L: %u-%u | C: %u-%u | R: %u-%u", leftLow, leftHigh, centerLow, centerHigh, rightLow, rightHigh);
}


/**
 * Calibrates LightSensors for Normalization usage by providing the values
 */
void CalibrateLightSensorsValues(int32_t newLeftLow, int32_t newLeftHigh, int32_t newCenterLow, int32_t newCenterHigh, int32_t newRightLow, int32_t newRightHigh)
{
	// overwrite values
	leftLow = newLeftLow;
	leftHigh = newLeftHigh;

	centerLow = newCenterLow;
	centerHigh = newCenterHigh;

	rightLow = newRightLow;
	rightHigh = newRightHigh;

	// recompute ranges
	leftRange = leftHigh - leftLow;
	centerRange = centerHigh - centerLow;
	rightRange = rightHigh - rightLow;
}

/**
 * return light sensor ADC value for a given sensor
 */
uint32_t GetLightSensorValue(LightSensor sensor)
{
	switch(sensor) {

		case LeftLightSensor: return GetAdcValue(5);
		case CenterLightSensor: return GetAdcValue(0);
		case RightLightSensor: return GetAdcValue(2);
	}

	return 0;
}

/**
 * return normalized light sensor ADC value for a given sensor
 */
float GetUnbufferedNormalizedLightSensorValue(LightSensor sensor)
{
	int32_t range = (1 << 12);
	int32_t offset = 0;

	// identify applicable value range and range offset information
	switch(sensor) {
		case LeftLightSensor:
			range = leftRange; offset = leftLow; break;
		case CenterLightSensor:
			range = centerRange; offset = centerLow; break;
		case RightLightSensor:
			range = rightRange; offset = rightLow; break;
	}

	// return normalized value
	return (float) ((int32_t) GetLightSensorValue(sensor) - offset) / (float) range;
}


/**
 * return buffered normalized light sensor ADC value for a given sensor
 */
float GetNormalizedLightSensorValue(LightSensor sensor)
{
	switch(sensor) {
		case LeftLightSensor:
			return normalizedLeft;
		case CenterLightSensor:
			return normalizedCenter;
		case RightLightSensor:
			return normalizedRight;
	}

	return 0.0f;
}

/**
 * Write light sensor values to a given buffer
 */
void WriteLightSensorValuesTo(uint32_t* buffer)
{
	buffer[0] = GetLightSensorValue(LeftLightSensor);
	buffer[1] = GetLightSensorValue(CenterLightSensor);
	buffer[2] = GetLightSensorValue(RightLightSensor);
}

/**
 * Write normalized light sensor values to a given buffer
 */
void WriteNormalizedLightSensorValuesTo(float* buffer)
{
	buffer[0] = GetUnbufferedNormalizedLightSensorValue(LeftLightSensor);
	buffer[1] = GetUnbufferedNormalizedLightSensorValue(CenterLightSensor);
	buffer[2] = GetUnbufferedNormalizedLightSensorValue(RightLightSensor);
}

/**
 * Updates the normalized light value buffers
 */
void UpdateNormalizedLightSensorValues() {

	normalizedLeft = GetUnbufferedNormalizedLightSensorValue(LeftLightSensor);
	normalizedCenter = GetUnbufferedNormalizedLightSensorValue(CenterLightSensor);
	normalizedRight = GetUnbufferedNormalizedLightSensorValue(RightLightSensor);
}


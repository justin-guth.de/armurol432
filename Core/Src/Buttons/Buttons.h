#ifndef BUTTONS_H
#define BUTTONS_H

#include <stdint.h>

typedef enum 
{ 
	LeftButton   = 0b001, 
	CenterButton = 0b010, 
	RightButton  = 0b100 
} Buttons;

int32_t GetPressedButtons();


#endif

#include "Buttons.h"

#include "stm32l4xx_hal.h"
#include "stm32l432xx.h"
#include "main.h"

/**
 * Returns a bitmap of the pressed buttons at a given moment
 */
int32_t GetPressedButtons() {

	int32_t result = 0;

	if (HAL_GPIO_ReadPin(switch_left_GPIO_Port,  switch_left_Pin)    == 0) result |= LeftButton;
	if (HAL_GPIO_ReadPin(switch_right_GPIO_Port, switch_right_Pin)   == 0) result |= RightButton;
	if (HAL_GPIO_ReadPin(switch_middle_GPIO_Port, switch_middle_Pin) == 0) result |= CenterButton;

	return result;
}

#ifndef ENCODER_H
#define ENCODER_H

#include <stdint.h>

// #define TIME_WINDOW 333

typedef enum { LeftEncoder, RightEncoder } Encoder;


uint32_t GetEncoderValue(Encoder sensor);
float GetNormalizedEncoderValue(Encoder sensor);
void WriteEncoderValuesTo(uint32_t* buffer);
void WriteNormalizedEncoderValuesTo(float* buffer);



typedef enum { Undefined, High, Low } WheelEncoderState;

WheelEncoderState GetWheelEncoderState(Encoder encoder);
void WriteWheelEncoderStates(WheelEncoderState* leftState, WheelEncoderState* rightState);
void UpdateWheelEncoderStates();

uint32_t GetEncoderPeriod(Encoder encoder);

uint32_t GetEncoderTotalTickCount(Encoder encoder);
uint32_t GetEncoderTickCountRelative(Encoder encoder, uint32_t ticks);

#endif

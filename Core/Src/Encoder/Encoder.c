
#include "Encoder.h"
#include "../ADC/AnalogDigitalConverter.h"
#include "../IO/IO.h"
#include "../Util/Util.h"
#include "../Timer/Timer.h"

/**
 * Returns the current adc content for a given encoder
 */
uint32_t GetEncoderValue(Encoder sensor)
{
	switch(sensor) {

		case LeftEncoder: return GetAdcValue(1);
		case RightEncoder: return GetAdcValue(4);
	}

	return 0;
}

/**
 * Returns a normalized float representation of a given encoder value
 */
float GetNormalizedEncoderValue(Encoder sensor)
{
	return (float) GetEncoderValue(sensor) / (1 << 12);
}

/**
 * Writes both encoder values to a buffer
 */
void WriteEncoderValuesTo(uint32_t* buffer)
{
	buffer[0] = GetEncoderValue(LeftEncoder);
	buffer[1] = GetEncoderValue(RightEncoder);
}

/**
 * Writes both normalized encoder values to a buffer
 */
void WriteNormalizedEncoderValuesTo(float* buffer)
{
	buffer[0] = GetNormalizedEncoderValue(LeftEncoder);
	buffer[1] = GetNormalizedEncoderValue(RightEncoder);
}


// last detected encoder states per encoder
static WheelEncoderState wheelEncoderLastStateLeft = Undefined;
static WheelEncoderState wheelEncoderLastStateRight = Undefined;

// ADC value threshold high per encoder
static uint32_t wheelEncoderHighThresholdLeft = 2500;
static uint32_t wheelEncoderHighThresholdRight = 2500;

// ADC value threshold low per encoder
static uint32_t wheelEncoderLowThresholdLeft = 500;
static uint32_t wheelEncoderLowThresholdRight = 500;

// total tick count per encoder
static uint32_t wheelEncoderTickCountLeft  = 0;
static uint32_t wheelEncoderTickCountRight = 0;

// period per encoder in milliseconds
static uint32_t wheelEncoderPeriodLeft  = 0;
static uint32_t wheelEncoderPeriodRight = 0;

// Whether timers have been reset already
int areTimersInitialized = 0;

// Timers storing encoder periods
static struct TimerStruct wheelEncoderTimerLeft;
static struct TimerStruct wheelEncoderTimerRight;

/**
 * Get current wheel encoder state for a given encoder
 */
WheelEncoderState GetWheelEncoderState(Encoder encoder) {
	switch(encoder) {

		case LeftEncoder:  return wheelEncoderLastStateLeft;  break;
		case RightEncoder: return wheelEncoderLastStateRight; break;
	}

	return Undefined;
}

/**
 * Write wheel encoder states into the given memory adresses
 */
void WriteWheelEncoderStates(WheelEncoderState* leftState, WheelEncoderState* rightState) {
	*leftState = GetWheelEncoderState(LeftEncoder);
	*rightState = GetWheelEncoderState(RightEncoder);
}

/**
 * Update wheel encoder states. Perform Schmitt trigger check and update tick counts and periods accordingly
 */
void UpdateWheelEncoderStates() {

	// Reset timers if required
	if (!areTimersInitialized) {
			ResetTimer(&wheelEncoderTimerLeft);
			ResetTimer(&wheelEncoderTimerRight);

			areTimersInitialized = 1;
	}

	// get current encoder values
	uint32_t currentWheelEncoderValueLeft = GetEncoderValue(LeftEncoder);
	uint32_t currentWheelEncoderValueRight = GetEncoderValue(RightEncoder);

	// LEFT: If high threshold is surpassed...
	if (currentWheelEncoderValueLeft > wheelEncoderHighThresholdLeft) {

		// and previous state was low:
		if (wheelEncoderLastStateLeft == Low) {

			// Count tick
			wheelEncoderTickCountLeft++;

			// update wheel encoder period and reset timer
			wheelEncoderPeriodLeft = GetElapsedTime(&wheelEncoderTimerLeft);
			ResetTimer(&wheelEncoderTimerLeft);
		}

		// Set last state to high
		wheelEncoderLastStateLeft = High;
	}
	// Else if the low threshold is passed...
	else if (currentWheelEncoderValueLeft < wheelEncoderLowThresholdLeft) {

		// and previous state was high:
		if (wheelEncoderLastStateLeft == High) {

			// Count tick
			wheelEncoderTickCountLeft++;

			// Don't update period as only rising edges are detected
		}


		// Set last state to low
		wheelEncoderLastStateLeft = Low;
	}

	// RIGHT: Analogous to left encoder
	if (currentWheelEncoderValueRight > wheelEncoderHighThresholdRight) {

		if (wheelEncoderLastStateRight == Low) {
			wheelEncoderTickCountRight++;

			wheelEncoderPeriodRight = GetElapsedTime(&wheelEncoderTimerRight);
			ResetTimer(&wheelEncoderTimerRight);
		}

		wheelEncoderLastStateRight = High;
	}
	else if (currentWheelEncoderValueRight < wheelEncoderLowThresholdRight) {

		if (wheelEncoderLastStateRight == High) {
			wheelEncoderTickCountRight++;
		}

		wheelEncoderLastStateRight = Low;
	}
}

/**
 * Get total tick counts for a given encoder since program start
 */
uint32_t GetEncoderTotalTickCount(Encoder encoder) {

	switch(encoder) {

		case LeftEncoder:  return wheelEncoderTickCountLeft;  break;
		case RightEncoder: return wheelEncoderTickCountRight; break;
	}

	return 0;
}

/**
 * Get relative tick count for a given encoder
 */
uint32_t GetEncoderTickCountRelative(Encoder encoder, uint32_t ticks) {

	switch(encoder) {

		case LeftEncoder:  return wheelEncoderTickCountLeft - ticks;  break;
		case RightEncoder: return wheelEncoderTickCountRight - ticks; break;
	}

	return 0;
}

/**
 * Get rising edge tick period for a given encoder
 */
uint32_t GetEncoderPeriod(Encoder encoder) {

	switch(encoder) {

		case LeftEncoder:  return wheelEncoderPeriodLeft;  break;
		case RightEncoder: return wheelEncoderPeriodRight; break;
	}

	return 0;
}

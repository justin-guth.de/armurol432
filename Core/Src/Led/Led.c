#include "Led.h"

#include "../Definitions.h"
#include "../IO/IO.h"

// Set one or more led either to on or off
void SetLedState(LedType type, LedState state) {

	// Declare fields
	GPIO_TypeDef* port     = NULL;
	uint16_t 	  pin      = LEFT_LED_GPIO_PIN;
	GPIO_PinState pinState = GPIO_PIN_RESET;


	// Set pinState value depending on given LedState
	switch(state) {
		case On:  pinState = GPIO_PIN_SET; break;
		case Off: pinState = GPIO_PIN_RESET; break;
	}

	// Update GPIO values if left led is selected
	if (type & Left) {
		port = LEFT_LED_GPIO_PORT;  pin = LEFT_LED_GPIO_PIN;
		HAL_GPIO_WritePin(port, pin, pinState);
	}

	// Update GPIO values if right led is selected
	if (type & Right) {
		port = RIGHT_LED_GPIO_PORT; pin = RIGHT_LED_GPIO_PIN;
		HAL_GPIO_WritePin(port, pin, pinState);
	}
}


#ifndef LED_H
#define LED_H

typedef enum { On, Off } LedState;
typedef enum { Left = 0b001, Right  = 0b010} LedType;

void SetLedState(LedType type, LedState state);




#endif

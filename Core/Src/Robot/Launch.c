#include "Launch.h"
#include "../Buttons/Buttons.h"
#include "../Util/Util.h"
#include "../Led/Led.h"
#include "../Motors/Motor.h"
#include "../ADC/AnalogDigitalConverter.h"
#include "../Encoder/Encoder.h"
#include "../LightSensors/LightSensors.h"

#include "main.h"

#include "../IO/IO.h"


/**
 * Launches a given controller and begins main loop
 */
void Launch(struct Controller* controller) {

	// Call OnInit handler
	controller->OnInit(controller);

	while (1) {

		// Update ADC values if applicable
		TryUpdateADCValues();

		TryRead();

		// Update wheel encoder states
		UpdateWheelEncoderStates();

		// Update light sensor buffer
		UpdateNormalizedLightSensorValues();

		// If a loop wait duration is set wait for that time
		if (controller->MillisecondsPerTick != 0)
		{
			Wait(controller->MillisecondsPerTick);
		}

		// Call OnUpdate handler
		controller->OnUpdate(controller);


		// Check collisions
		int32_t pressedButtons = GetPressedButtons();

		if (pressedButtons != 0)
		{
			Log("Collisions detected... (Code: %i)\n", pressedButtons);
			controller->OnCollision(controller, pressedButtons);
		}

		// Increment UpdateCount debug info
		controller->UpdateCount++;
 	}
}

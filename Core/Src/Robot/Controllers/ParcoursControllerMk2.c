#include "ParcoursControllerMk2.h"

#include "../../Util/Util.h"
#include "../../LightSensors/LightSensors.h"
#include "../../Timer/Timer.h"
#include "../../Encoder/Encoder.h"
#include "../../IO/IO.h"
#include "../../Motors/Motor.h"
#include "../../Led/Led.h"


// calibrated low and high values for the line sensors by measuring
const static int32_t leftLow = 1250;
const static int32_t leftHigh = 3600;

const static int32_t centerLow = 1680;
const static int32_t centerHigh = 3660;

const static int32_t rightLow = 250;
const static int32_t rightHigh = 3300;

static volatile uint8_t buffer[16];

/**
 * Initialize robot
 * short timeout
 * calibrate light sensor values identified by trial and error
 */
static void OnInit(struct Controller *controller) {

	// calibrate light sensors module
	CalibrateLightSensorsValues(leftLow, leftHigh, centerLow, centerHigh, rightLow, rightHigh);
	buffer[15] = 0;
	Wait(1000);
	SetLedState(Left|Right, Off);
}

/**
 * Handles general update logic
 * Dispatches the cooperative multitasking tasks
 */
static void OnUpdate(struct Controller *controller) {

	static uint8_t buffer[256];
	WriteReceived(buffer, 256);

	float leftSpeed = ((float) buffer[0]) / 0xFF;
	float rightSpeed = ((float) buffer[1]) / 0xFF;

	SetMotorState(LeftMotor, Forward, leftSpeed);
	SetMotorState(RightMotor, Forward, rightSpeed);
}


/**
 * Collision handler
 * simply jump into the obstacle section once a button is pressed
 */
static void OnCollision(struct Controller *controller, Buttons buttons) {

}

/**
 * configuration logic for this controller
 * simply set update timeout to 0
 */
static void Configure(struct Controller *controller) {
	controller->MillisecondsPerTick = 0;
}



// Place initialization logic macro
PLACE_INIT_LOGIC(ParcoursControllerMk2InitState)

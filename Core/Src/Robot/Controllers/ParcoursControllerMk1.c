#include "ParcoursControllerMk1.h"

#include "../../Util/Util.h"
#include "../../LightSensors/LightSensors.h"
#include "../../Timer/Timer.h"
#include "../../Encoder/Encoder.h"
#include "../../IO/IO.h"
#include "../../Motors/Motor.h"
#include "../../Led/Led.h"

// This file implements a first iteration of a robot controller.
// Ironically it is also the only one I needed to write making the entire controller mechanism superfluous

// Configutation preprocessor definitions:

#define CONFIG_LINE_DETECT_MINIMUM_THRESHOLD 0.99f
#define CONFIG_LINE_LOSS_MAXIMUM_THRESHOLD 0.6f
#define CONFIG_LINE_FOLLOW_MOTOR_SPEED_SET_VALUE 0.99f
#define CONFIG_LINE_FOLLOW_PROPORTIONAL_CONSTANT 4.0f
#define CONFIG_LINE_FOLLOW_DIFFERENTIAL_CONSTANT 40.0f

#define CONFIG_LINE_SWEEP_INITIAL_DISTANCE 1
#define CONFIG_LINE_SWEEP_REPEATED_DISTANCE 130
#define CONFIG_LINE_SWEEP_ANGLE 69

#define CONFIG_DRIVE_STRAIGHT_SET_VALUE 1.0f;
#define CONFIG_TURN_SET_VALUE 1.0f;

#define CONFIG_OBSTACLE_RADIUS 45
// #define CONFIG_PATH "D:500;T:-150;D:355;T:90;D:310;"
#define CONFIG_PATH "D:180;T:-90;D:180;T:60;D:434;"

#define ROBOT_LENGTH 100

// Define all possible parcours sections. Defines what cooperative main task should be executed at a given iteration
typedef enum {
	HardCodedSection,
	LineFollowSection,
	InterruptSection,
	ObstacleSection
} ParcoursSection;



// keep track of the current parcours section starting from the first hardcoded part
//static ParcoursSection parcoursSection = Straight0;
static ParcoursSection parcoursSection = HardCodedSection;

// Variables for keeping track of the last measured wheel tick counts
// These variables are reset between each DriveStraight and Turn call
static uint32_t lastTickCountLeft = 0;
static uint32_t lastTickCountRight = 0;

// calibrated low and high values for the line sensors by measuring
const static int32_t leftLow = 1250;
const static int32_t leftHigh = 3600;

const static int32_t centerLow = 1680;
const static int32_t centerHigh = 3660;

const static int32_t rightLow = 250;
const static int32_t rightHigh = 3300;


// Fields used for turn inclination computation
static float turnInclinationThreshold = -0.07f;
static float turnInclinations[20];
static int turnInclinationIndex = 0;
static Timer turnInclinationTimer;
static int turnInclination = 1;

/**
 * Reset the global tick counters
 */
void ResetLastTickCounts() {

	lastTickCountLeft = GetEncoderTotalTickCount(LeftEncoder);
	lastTickCountRight = GetEncoderTotalTickCount(RightEncoder);
}

/**
 * drive straight for a given amount of millimeters
 *
 * returns wether the drive straight function should be called again (1)
 * or if the operation is completed (0)
 *
 * Make sure to reset the last tick counts before starting a new leg
 */
int DriveStraight(int mm) {

	// compute the millimeters driven per tick by fine tuning geometric estimates with trial and error
	const static float mmPerTick = 39.0f * 3.2f / 24.0f;

	// store direction in a variable because why not
	const MotorDirection direction = mm > 0 ? Forward: Backward;

	// keep track of how many encoder ticks happened while travelling
	int ticksTraveled = 0;

	// The value "aimed for" the motors
	float setValue = CONFIG_DRIVE_STRAIGHT_SET_VALUE;

	// The later corrected values for both motors
	float rightValue = CONFIG_DRIVE_STRAIGHT_SET_VALUE;
	float leftValue = CONFIG_DRIVE_STRAIGHT_SET_VALUE;

	// counter for the encoder ticks that were counted left and right
	static uint32_t leftTicks = 0;
	static uint32_t rightTicks = 0;

	// set the ticks travelled by retrieving the relative tick count for the left wheel
	ticksTraveled = GetEncoderTickCountRelative(LeftEncoder, lastTickCountLeft);

	// compute the amount of elapsed millimeters
	float mmTraveled = (float) ticksTraveled * mmPerTick;

	// stop condition. If mm travelled is more than those passed
	if (mmTraveled > Abs(mm)) {

		// stop the motors and return false
		SetMotorState(LeftMotor, Standstill, 0.0f);
		SetMotorState(RightMotor, Standstill, 0.0f);

		return 0;
	}

	// Retrieve the tick counts for left and right relative to the last tick counts
	leftTicks = GetEncoderTickCountRelative(LeftEncoder, lastTickCountLeft);
	rightTicks = GetEncoderTickCountRelative(RightEncoder, lastTickCountRight);

	// Introduce a bias for the left wheel since my robot's wheels have different circumferences
	// identified by trial and error
	const float bias = 1.015f;

	// proportional constant
	const float Kp = 0.05;

	// get difference between left and right ticks
	float diff = ((float) leftTicks) * bias - (float) rightTicks;

	// adjust the right and left set values
	rightValue = setValue + Kp * diff;
	leftValue = setValue - Kp * diff;


	// Set the motor state using left and right set values
	SetMotorState(LeftMotor, direction, leftValue);
	SetMotorState(RightMotor, direction, rightValue);

	return 1;
}

/**
 * turn for a given amount of degrees (counterclockwise)
 *
 * returns wether the turn function should be called again (1)
 * or if the operation is completed (0)
 *
 * Make sure to reset the last tick counts before starting a new leg

 */
int Turn(int angle) {

	// How many angles are turned per measured wheel tick
	// determined by trial and error
	const static float anglePerTick = 7.5f;

	// Set motor directions according to whether the angle is positive or negative
	MotorDirection leftMotorDirection = angle > 0 ? Backward : Forward;
	MotorDirection rightMotorDirection = angle > 0 ? Forward : Backward;

	// stores how many ticks were travelled
	int ticksTraveled = 0;


	// desired wheel speed
	float setValue = CONFIG_TURN_SET_VALUE;

	// store adjusted wheel speed
	float rightValue = CONFIG_TURN_SET_VALUE;
	float leftValue = CONFIG_TURN_SET_VALUE;

	// store elapsed ticks for left and right encoder
	static uint32_t leftTicks = 0;
	static uint32_t rightTicks = 0;

	// store how many ticks were traveled by the left wheel
	// used for stop condition
	ticksTraveled = GetEncoderTickCountRelative(LeftEncoder, lastTickCountLeft);

	// compute angle that was turned
	float angleTurned = (float) ticksTraveled * anglePerTick;

	// if the angle surpasses the given angle parameter stop and return
	if (angleTurned > Abs(angle)) {

		SetMotorState(LeftMotor, Standstill, 0.0f);
		SetMotorState(RightMotor, Standstill, 0.0f);

		return 0;
	}

	// get relative ticks for left and right wheel
	leftTicks = GetEncoderTickCountRelative(LeftEncoder, lastTickCountLeft);
	rightTicks = GetEncoderTickCountRelative(RightEncoder, lastTickCountRight);

	// bias for different circumferences
	const float bias = 1.0065f;

	// proportional constant
	const float Kp = 0.02;

	// compute difference between ticks
	float diff = ((float) leftTicks) * bias - (float) rightTicks;


	// adjust the right and left set values
	rightValue = setValue + Kp * diff;
	leftValue = setValue - Kp * diff;

	// Set motor states
	SetMotorState(LeftMotor, leftMotorDirection, leftValue);
	SetMotorState(RightMotor, rightMotorDirection, rightValue);

	return 1;
}

/**
 * Returns wether a line is detected by one of the light sensors
 */
int IsLineDetected() {

	// retrieve normalized light sensor values
	float normalizedLeft = GetNormalizedLightSensorValue(LeftLightSensor);
	float normalizedCenter = GetNormalizedLightSensorValue(CenterLightSensor);
	float normalizedRight = GetNormalizedLightSensorValue(RightLightSensor);

	// set an arbitrary threshold identified by trial and error
	const static float lightThreshold = CONFIG_LINE_DETECT_MINIMUM_THRESHOLD;

	// return wether one light sensor measures a value above that threshold
	return normalizedLeft > lightThreshold || normalizedCenter> lightThreshold || normalizedRight > lightThreshold;

}

/**
 * Procedure to follow a line by reading light sensor values and adjusting motor speeds accordingly
 * Returns the next parcours state to be in
 */
static ParcoursSection FollowLine() {

	// keep track of last error for differential regulation component
	static float lastError = 0.0f;

	// Timer for outputting periodic debug information
	static Timer timer;

	// retrieve normalized light values
	float normalizedLeft = GetNormalizedLightSensorValue(LeftLightSensor);
	float normalizedCenter = GetNormalizedLightSensorValue(CenterLightSensor);
	float normalizedRight = GetNormalizedLightSensorValue(RightLightSensor);

	// Every 2 seconds output the current normalized readings on the serial bus
	OnTimeElapsedPeriodic(&timer, 2000) {

		Log("L: %d, C: %d, R: %d\n",
				(int32_t) (1000 * normalizedLeft),
				(int32_t) (1000 * normalizedCenter),
				(int32_t) (1000 * normalizedRight));
	}

	// detect possible line interruption
	// arbitrary low threshold identified by trial and error
	const static float lightThreshold = CONFIG_LINE_LOSS_MAXIMUM_THRESHOLD;

	// if all sensors measure below that threshold return line lost state (InterruptSection)
	if (normalizedLeft < lightThreshold && normalizedCenter < lightThreshold
			&& normalizedRight < lightThreshold) {
		return InterruptSection;
	}

	// Set value and regulation constants identified by trial and error
	const static float setValue = CONFIG_LINE_FOLLOW_MOTOR_SPEED_SET_VALUE;
	const static float Kp = CONFIG_LINE_FOLLOW_PROPORTIONAL_CONSTANT;
	const static float Kd = CONFIG_LINE_FOLLOW_DIFFERENTIAL_CONSTANT;

	// compute the error by subtracting left from right readings
	// use the center light readings to amplify the error between 1 and 2 times
	// amplification = (2 - c^2) where c should roughly be between 0 and 1
	float error = (normalizedRight - normalizedLeft) * (2.0f - normalizedCenter * normalizedCenter);

	// compute diff to apply to the set value by factoring in proportional and differential error
	float diff = Kp * error + Kd * (error - lastError);

	OnTimeElapsedPeriodic(&turnInclinationTimer, 50) {

		// Store last 20 measured left right light sensor differences
		// Rotate through array
		// Use the sum of these values to compute a turn inclination.
		// This is useful for losing the line on sharp turns, if the line is lost on a left turn the first sweep goes to the left
		// analogous for right
		turnInclinations[(turnInclinationIndex++) % 20] = (normalizedRight - normalizedLeft);


		float turnInclinationsSum = 0.0f;

		for (int i = 0; i < 20; i++) {
			turnInclinationsSum += turnInclinations[i];
		}

		// Log("%d\n", (int) (turnInclinationsSum * 1000));

		turnInclination = turnInclinationsSum > turnInclinationThreshold ? -1 : 1;

	}


	// update last error reading
	lastError = error;

	// Check if a wheel direction reversal is required for the correction
	if (diff > setValue) {

		SetMotorState(LeftMotor, Forward, setValue + diff);
		SetMotorState(RightMotor, Backward, -(setValue - diff));
	} else if (diff < -setValue) {

		SetMotorState(LeftMotor, Backward, -(setValue + diff));
		SetMotorState(RightMotor, Forward, setValue - diff);
	} else {

		// General case, no motor direction reversal necessary
		SetMotorState(LeftMotor, Forward, setValue + diff);
		SetMotorState(RightMotor, Forward, setValue - diff);
	}

	// If the difference is minimally significant use led to indicate corrections
	if (Abs(diff) > 0.05f) {

		SetLedState(Left, diff > 0.0f ? On : Off);
		SetLedState(Right, diff > 0.0f ? Off : On);
	}
	else{

		SetLedState(Left | Right, Off);
	}

	// Return the same state as the line has not been lost
	return LineFollowSection;
}

/**
 * Handles what to do if no line has been detected below the robot
 * returns the next parcours section to handle
 */
static ParcoursSection HandleLineInterrupt() {

	// defines what components of the sweeping process exist
	typedef enum { SweepInit, SweepLeft, SweepRight, SweepBack, SweepForward } SweepState;

	// stores the current sweep state
	static SweepState sweepState;

	// defines wether the HandleLineInterrupt function has already been called once without having found a line since
	typedef enum {
		BeforeInterruptHandled, InterruptHandled
	} InterruptHandlingStage;

	// used to check if this is the first part of the sweeping process (one time only 2cm forward)
	static InterruptHandlingStage interruptHandlingStage = BeforeInterruptHandled;

	// Indicate lost line by turning on both leds
	SetLedState(Left | Right, On);

	// if this is the first iteration since losing the line set the stage to init and reset tick counts
	if (interruptHandlingStage == BeforeInterruptHandled) {

		sweepState = SweepInit;
		ResetLastTickCounts();
	}

	// InterruptHandled acts as a between state before restarting the process and after finishing
	interruptHandlingStage = InterruptHandled;


	/**
	 * Process:
	 * 		drive CONFIG_LINE_SWEEP_INITIAL_DISTANCE mm forward
	 * 			|
	 * 			V
	 * 		turn left CONFIG_LINE_SWEEP_ANGLE° <-------------------------
	 * 			|														|
	 * 			V														|
	 * 		turn right 2*CONFIG_LINE_SWEEP_ANGLE°						|
	 * 			|														|
	 * 			V														|
	 * 		turn left CONFIG_LINE_SWEEP_ANGLE°							|
	 * 			|														|
	 * 			V														|
	 *		drive CONFIG_LINE_SWEEP_REPEATED_DISTANCE mm forward -------|
	 *
	 */
	switch (sweepState) {

		case SweepInit:

			if (!DriveStraight(20)){
				sweepState = SweepLeft;
				ResetLastTickCounts();
			}
			break;

		case SweepLeft:

			if (!Turn(turnInclination * CONFIG_LINE_SWEEP_ANGLE)){
				sweepState = SweepRight;
				ResetLastTickCounts();
			}
			break;

		case SweepRight:

			if (!Turn(-2* turnInclination *CONFIG_LINE_SWEEP_ANGLE)) {
				sweepState = SweepBack;
				ResetLastTickCounts();
			}
			break;

		case SweepBack:

			if (!Turn(turnInclination * CONFIG_LINE_SWEEP_ANGLE)) {
				sweepState = SweepForward;
				ResetLastTickCounts();
			}
			break;

		case SweepForward:

			if (!DriveStraight(CONFIG_LINE_SWEEP_REPEATED_DISTANCE)) {
				sweepState = SweepLeft;
				ResetLastTickCounts();
			}

			break;
	}

	// if at any point a line is found reset handling stage and return the line follow section
	if (IsLineDetected()) {
		interruptHandlingStage = BeforeInterruptHandled;
		return LineFollowSection;
	}

	// if line has not been detected return the same state
	return InterruptSection;
}

/**
 * Defines procedure to handle Obstacle circumnavigation
 * returns the next parcours state to handle after this iteration
 */
static ParcoursSection HandleObstacleSection() {

	// How far the obstacle should be navigated around
	const static int driveRadius = CONFIG_OBSTACLE_RADIUS;

	// defines the different circumnavigation legs
	typedef enum { CircumventStart, CircumventDriveBack, CircumventTurnRight, CircumventDrive1, CircumventTurnLeft1, CircumventDrive2, CircumventTurnLeft2, CircumventDrive3, CircumventAlign } CircumventionState;

	// stores the current leg
	static CircumventionState circumventionState = CircumventStart;

	// Timer for periodic led toggling
	static Timer timer;

	// simple counter for setting led values
	static int ledStates = 0;

	// Every half second change the led states
	OnTimeElapsedPeriodic(&timer, 500) {

		SetLedState(Left, ledStates % 2);
		SetLedState(Right, (ledStates + 1) % 2);
		++ledStates;
	}

	// when leg is start immediately change into turn right and reset ticks
	if (circumventionState == CircumventStart) {

		circumventionState = CircumventDriveBack;
		ResetLastTickCounts();
	}

	/**
	 * handle all circumvention states
	 *
	 * the robot takes a turn around the obstacle on the right hand side
	 *
	 *         |
	 *         |
	 *         |
	 *    ------
	 *    |  ####
  	 *    | ######
	 *    | ######
	 *    |  ####
	 *    -----|
	 *         |
	 *         |
	 *         V
	 */
	switch (circumventionState) {

		case CircumventDriveBack:

			if (!DriveStraight(-10)){
				circumventionState = CircumventTurnRight;
				ResetLastTickCounts();
			}
			break;

		case CircumventTurnRight:

			if (!Turn(-60)){
				circumventionState = CircumventDrive1;
				ResetLastTickCounts();
			}
			break;


		case CircumventDrive1:

			if (!DriveStraight((int) Sqrt(2 * (driveRadius + 42) * (driveRadius + 42)))){
				circumventionState = CircumventTurnLeft1;
				ResetLastTickCounts();
			}
			break;

		case CircumventTurnLeft1:

			if (!Turn(60)){
				circumventionState = CircumventDrive2;
				ResetLastTickCounts();
			}
			break;

		case CircumventDrive2:

			if (!DriveStraight(driveRadius + ROBOT_LENGTH + 10)){
				circumventionState = CircumventTurnLeft2;
				ResetLastTickCounts();
			}
			break;

		case CircumventTurnLeft2:

			if (!Turn(60)){
				circumventionState = CircumventDrive3;
				ResetLastTickCounts();
			}
			break;

		case CircumventDrive3:

			if (!DriveStraight((int) Sqrt(2 * (driveRadius + 42) * (driveRadius + 42)))){
				circumventionState = CircumventAlign;
				ResetLastTickCounts();
			}

			// if line is found jump back into the line follow section
			if (IsLineDetected()) {
				circumventionState = CircumventStart;
				ResetLastTickCounts();

				return LineFollowSection;
			}

			break;

		case CircumventAlign:

			// once circumvention is finished or line is found jump back into the line follow section
			if (!Turn(-60) || IsLineDetected()) {
				circumventionState = CircumventStart;
				ResetLastTickCounts();

				turnInclination = -1;
				return LineFollowSection;
			}
			break;

		default: break;
	}

	// if not finished return the same parcours section
	return ObstacleSection;
}

/**
 * Interprets a program given as a c string
 * Syntax: ([movement_class]:[parameter];)+
 * movement class may be D or T for Driving straight or turning
 * the parameter may be positive or negative
 * Example:
 *
 * 	D:100;T:-90;   // Drive 100mm forward, then turn 90° right
 */
int InterpretProgram(const char* program) {

	// stores the next expected symbol by the parser
	typedef enum { ExpectMovementClass, ExpectColon, ExpectParameterOrSign, ExpectParameter, ExpectParameterOrSemicolon, Error } InterpreterState;
	// describes a movement type, driving straight or turning
	typedef enum {MovementClassDriveStraight, MovementClassTurn, MovementClassUndefined } MovementClass;

	// stores the current parser state
	static InterpreterState currentState = ExpectMovementClass;

	// stores the current movement class
	static MovementClass currentMovementClass = MovementClassUndefined;

	// stores the current position within the program string
	static int32_t currentPosition = -1;

	// stores the current symbol at the current position
	static char currentSymbol = -1;

	// buffer for the current parameter
	static int currentParameter = 0;
	// stores the current sign (either 1 or -1)
	static int currentParameterSign = 1;

	// stores whether a current leg is being executed
	static int execute = 0;

	// as long as there is more stuff to read AND no leg is being executed
	while (program[currentPosition + 1] != 0 && !execute) {

		// go to next position in program
		++ currentPosition;

		// retrieve current symbol
		currentSymbol = program[currentPosition];

		Log("Parsing symbol: %c\n", currentSymbol);

		// differentiate parser state
		switch (currentState) {

			case ExpectMovementClass:
				{
					if (currentSymbol == 'D') {
						currentMovementClass = MovementClassDriveStraight;
						currentState = ExpectColon;
					} else if( currentSymbol == 'T') {
						currentMovementClass = MovementClassTurn;
						currentState = ExpectColon;
					} else  {
						currentMovementClass = MovementClassUndefined;
						Log("Unexpected Movement class: %c at position %d\n", currentSymbol, currentPosition);
						// return error flag
						currentState = Error;
						return -1;
					}
					// go to next state

				}
				break;
			case ExpectColon:
				{
					// check for colon
					if (currentSymbol == ':') {
						// go to next state
						currentState = ExpectParameterOrSign;
					} else {
						Log("Expected colon but got %c at position %d\n", currentSymbol, currentPosition);
						// return error flag
						currentState = Error;
						return -1;
					}
				}
				break;
			case ExpectParameterOrSign:
				{
					// if number starts without sign set parameter buffer to given value
					if (currentSymbol >= 48 && currentSymbol <= 57) {
						currentParameter = currentSymbol - 48;
						currentState = ExpectParameterOrSemicolon;
					}
					// if number starts with sign set parameter sign accordingly and go to expect parameter state
					else if (currentSymbol == '-') {
						currentParameterSign = -1;
						currentState = ExpectParameter;
					}else {
						Log("Expected parameter or sign but got %c at position %d\n", currentSymbol, currentPosition);
						// return error flag
						currentState = Error;
						return -1;
					}
				}
				break;
			case ExpectParameter:
				{
					// if number is provided set parameter buffer
					if (currentSymbol >= 48 && currentSymbol <= 57) {
						currentParameter = currentSymbol - 48;
						currentState = ExpectParameterOrSemicolon;
					} else {
						Log("Expected parameter but got %c at position %d\n", currentSymbol, currentPosition);
						// return error flag
						currentState = Error;
						return -1;
					}
				}
				break;
			case ExpectParameterOrSemicolon:
				{
					// if number is read shift buffer by an order of magnitude and add digit
					if (currentSymbol >= 48 && currentSymbol <= 57) {
						currentParameter *= 10;
						currentParameter += (currentSymbol - 48);
						currentState = ExpectParameterOrSemicolon;
					}
					// if semicolon is read start from start state again but set execute flag
					else if (currentSymbol == ';') {
						currentState = ExpectMovementClass;
						execute = 1;
					}
					else {
						Log("Expected parameter or semicolon but got %c at position %d\n", currentSymbol, currentPosition);
						// return error flag
						currentState = Error;
						return -1;
					}
				}
				break;

			default:

				Log("An error occured!");
				break;
		}
	}

	// If a leg should be executed
	if (execute) {

		// differentiate what operation to perform
		if (currentMovementClass == MovementClassDriveStraight) {

			// drive and if finished driving straight...
			if (!DriveStraight(currentParameterSign * currentParameter)) {

				Log("Finished driving for %dmm \n", currentParameterSign * currentParameter);

				// reset the parser state and reset execute flag
				currentState = ExpectMovementClass;
				currentMovementClass = MovementClassUndefined;
				currentParameter = 0;
				currentParameterSign = 1;
				execute = 0;
				ResetLastTickCounts();
			}
		}
		else if (currentMovementClass == MovementClassTurn) {

			// turn and if finished turning...
			if (!Turn(currentParameterSign * currentParameter)) {

				Log("Finished turning for %d°\n", currentParameterSign * currentParameter);

				// reset the parser state and reset execute flag
				currentState = ExpectMovementClass;
				currentMovementClass = MovementClassUndefined;
				currentParameter = 0;
				currentParameterSign = 1;
				execute = 0;
				ResetLastTickCounts();
			}
		}
		else {
			ResetLastTickCounts();
		}

	}

	// if end of string is reached return done flag
	else if (program[currentPosition + 1] == 0) {
		return 1;
	}

	// return not yet done flag
	return 0;
}



/**
 * Initialize robot
 * short timeout
 * calibrate light sensor values identified by trial and error
 */
static void OnInit(struct Controller *controller) {

	// calibrate light sensors module
	CalibrateLightSensorsValues(leftLow, leftHigh, centerLow, centerHigh, rightLow, rightHigh);
	Wait(1000);
	ResetLastTickCounts();

	for (int i = 0; i < 20; i++) {
		turnInclinations[i] = 0.0f;
	}
}

/**
 * Handles general update logic
 * Dispatches the cooperative multitasking tasks
 */
static void OnUpdate(struct Controller *controller) {

	switch (parcoursSection) {
		case HardCodedSection:
			if (InterpretProgram(CONFIG_PATH) == 1) {

				parcoursSection = LineFollowSection;
			}
			break;
		case LineFollowSection:

			parcoursSection = FollowLine();
			break;

		case ObstacleSection:

			parcoursSection = HandleObstacleSection();
			break;


		case InterruptSection:

			parcoursSection = HandleLineInterrupt();
			break;
		default:
			break;
	}
}

/**
 * Collision handler
 * simply jump into the obstacle section once a button is pressed
 */
static void OnCollision(struct Controller *controller, Buttons buttons) {

	parcoursSection = ObstacleSection;
}

/**
 * configuration logic for this controller
 * simply set update timeout to 0
 */
static void Configure(struct Controller *controller) {
	controller->MillisecondsPerTick = 0;
}












// Place initialization logic macro
PLACE_INIT_LOGIC(ParcoursControllerMk1InitState)

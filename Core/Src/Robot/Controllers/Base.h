#include "../Controller.h"

// QOL macro that creates a function implementation for a given controller
// This function automatically links the handler functions in a given translation unit to the returned controller.
// This requires that an OnUpdate, OnCollision and OnInit function are defined withing that translation unit!
#define PLACE_INIT_LOGIC(NAME) struct Controller* NAME() \
{ \
	struct Controller* controller = InitController(); \
\
	controller->OnUpdate = OnUpdate; \
	controller->OnCollision = OnCollision; \
	controller->OnInit = OnInit; \
\
	Configure(controller); \
\
	return controller; \
} 

// QOL macro that creates a function declaration for a given controller
#define DECLARE_INIT_LOGIC(NAME) struct Controller* NAME();
 

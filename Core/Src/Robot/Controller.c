#include "Controller.h"

#include "../IO/IO.h"

static struct Controller controller;

static void DefaultOnUpdate(struct Controller* controller)
{
}


static void DefaultOnInit(struct Controller* controller)
{
}

static void DefaultOnCollision(struct Controller* controller, Buttons buttons)
{
	Log("Collision Detected!");
	if (buttons & LeftButton)   Log(" [Left]");
	if (buttons & CenterButton) Log(" [Center]");
	if (buttons & RightButton)  Log(" [Right]");
	Log("\n");
}

/**
 * Initializes a static controller with default logic and returns a pointer to it
 *
 */
struct Controller* InitController()
{
	Log("Initializing State at %#012x!\n", &controller);

	controller.MillisecondsPerTick = 10;
	controller.UpdateCount = 0;

	controller.OnUpdate = DefaultOnUpdate;
	controller.OnCollision = DefaultOnCollision;
	controller.OnInit = DefaultOnInit;

	return &controller;
}

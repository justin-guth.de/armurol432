#ifndef STATE_H
#define STATE_H


#include <stdint.h>

#include "../Buttons/Buttons.h"

struct Controller
{
	// Counts how many updates have occured. Mainly useful for debug purposes or doing something every nth update
	uint32_t UpdateCount;
	
	// How many milliseconds to wait after each update loop iteration. Set to 0 for no waiting.
	uint32_t MillisecondsPerTick;

	// Function Callback for what to do on an update
	void(*OnUpdate)(struct Controller*);
	
	// Function Callback for what to do on init
	void(*OnInit)(struct Controller*);

	// Function Callback for what to do on collision detection
	void(*OnCollision)(struct Controller*, Buttons);

};

extern struct Controller state;

/**
* Initializez a robot state
*/
struct Controller*  InitController();

#endif
